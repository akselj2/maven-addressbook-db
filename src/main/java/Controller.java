import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

import java.sql.*;

public class Controller {
    Model myModel;

    @FXML
    TextField textFieldName;

    @FXML
    TextField textFieldStreet;

    @FXML
    TextField textFieldPlz;

    @FXML
    ObservableList<String> names = FXCollections.observableArrayList();

    @FXML
    ListView<String> databaseListView = new ListView<>();

    // binds textfield text properties to the different properties in Model class

    public void setModel(Model model) {
        databaseListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            handleTextFields();
        });

        myModel = model;

        textFieldName.textProperty().bindBidirectional(model.nameProperty());
        textFieldStreet.textProperty().bindBidirectional(model.streetProperty());
        textFieldPlz.textProperty().bindBidirectional(model.plzProperty(), new NumberStringConverter());
        showItems();

        databaseListView.setItems(names);
    }

    private void handleTextFields() {
        int index = databaseListView.getSelectionModel().getSelectedIndex();
        if (index < 0) index = 0;
        System.out.println(getFromDataBase("name", index));
        textFieldName.setText(getFromDataBase("name", index));
        textFieldStreet.setText(getFromDataBase("street", index));
        textFieldPlz.setText(getFromDataBase("plz", index));
    }

    // sets all fields to blank

    public void clear(ActionEvent event) {
        textFieldName.setText("");
        textFieldStreet.setText("");
        textFieldPlz.setText("");
    }

    // once button is clicked, method calls insertFromView() from Model class and sets Text for a Label to "Check Db" and makes it Green.

    public void add(ActionEvent event) {
        if (checkInputFields()) {
            myModel.insertFromView();

            handleTextFields();
            showItems();
            //clear(event);
        }
    }

    // checks if all textFields are empty. if false, it updates all items.

    public void edit() {
        if (checkInputFields()) {
            String name = textFieldName.getText();
            String street = textFieldStreet.getText();
            int plz = Integer.parseInt(textFieldPlz.getText());
            String selectedName = databaseListView.getSelectionModel().getSelectedItem();

            /**
             * selectedName is 1st
             * name is second
             * street is third
             * plz is fourth.
             */

            myModel.edit(selectedName, name, street, plz);

            showItems();
        }
    }

    public boolean checkInputFields() {
        return isNotNullAndNotEmpty(textFieldName.getText()) && isNotNullAndNotEmpty(textFieldStreet.getText()) && isNotNullAndNotEmpty(textFieldPlz.getText());
    }

    private boolean isNotNullAndNotEmpty(String string) {
        return (string != null && !string.equals(""));
    }

    public void delete() {
        delete(databaseListView.getSelectionModel().getSelectedItem());
    }

    public void delete(String name) {

        String sql = "DELETE FROM addresses WHERE name='" + name + "'";

        Connection myConn = connect();

        try {
            Statement stmt = myConn.createStatement();

            stmt.executeQuery(sql);
            names.remove(name);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void showItems() {

        ResultSet rs = getAllFromDataBase("name");

        int i = databaseListView.getSelectionModel().getSelectedIndex();
        names.clear();

        try {
            while (rs.next()) {
                String name = rs.getString(1);
                names.add(name);
            }
            databaseListView.getSelectionModel().select(i);
        } catch (SQLException e) {
            throw new RuntimeException();
        }

    }

    //

    private String getFromDataBase(String commandPart, int index) {
        String sql = "SELECT " + commandPart + " FROM addresses";
        try {
            Connection myConn = connect();
            Statement stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            ResultSet rs = stmt.executeQuery(sql);

            int i = 0;
            while (rs.next()) {

                String value = rs.getString(1);

                if (i == index) {
                    return value;
                }
                i++;
            }
            if (i == 0) {
                textFieldName.setText("");
                textFieldStreet.setText("");
                textFieldPlz.setText("0");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return "";
    }

    private ResultSet getAllFromDataBase(String commandPart) {
        String sql = "SELECT " + commandPart + " FROM addresses";
        try {
            Connection myConn = connect();
            Statement stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            return stmt.executeQuery(sql);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Connection connect() {
        Connection myConn = null;

        try {
            myConn = DriverManager.getConnection("jdbc:mariadb://localhost:3306/addressbook?user=root&password=aksel");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return myConn;
    }
}